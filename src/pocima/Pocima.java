package pocima;

import animal.Animal;
import personaje.Personaje;

public abstract class Pocima {

	public abstract void aplicarPocimaConSusEfectos(Animal animal,Personaje personaje);
	public abstract TipoDePocima getTipoPocima();

	
	
}
