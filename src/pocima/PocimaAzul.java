package pocima;

import animal.Animal;
import personaje.Personaje;

public class PocimaAzul extends Pocima {

	private double incrementoResistenciaAnimalPropio;
	private double disminucionResistenciaAnimalAjeno;

	public PocimaAzul(double incrementoResistenciaAnimalPropio, double disminucionResistenciaAnimalAjeno) {
		this.incrementoResistenciaAnimalPropio = incrementoResistenciaAnimalPropio;
		this.disminucionResistenciaAnimalAjeno = disminucionResistenciaAnimalAjeno;
	}

	public void setResistenciaAnimalPropio(double incrementoResistenciaAnimalPropio) {
		this.incrementoResistenciaAnimalPropio = incrementoResistenciaAnimalPropio;
	}

	public void setResistenciaAnimalAjeno(double disminucionResistenciaAnimalAjeno) {
		this.disminucionResistenciaAnimalAjeno = disminucionResistenciaAnimalAjeno;
	}

	public double getIncrementoResistenciaAnimalPropio() {
		return incrementoResistenciaAnimalPropio;
	}

	public double getDisminucionResistenciaAnimalAjeno() {
		return disminucionResistenciaAnimalAjeno;
	}

	@Override
	public void aplicarPocimaConSusEfectos(Animal animal, Personaje personaje) {

		if (personaje.esElDuenio(animal)) {
			animal.aumentarResistencia(incrementoResistenciaAnimalPropio);
		} else {
			animal.disminuirResistencia(getDisminucionResistenciaAnimalAjeno());
		}
	}

	@Override
	public TipoDePocima getTipoPocima() {
		return TipoDePocima.AZULES;
	}

}
