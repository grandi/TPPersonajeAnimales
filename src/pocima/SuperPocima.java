package pocima;

import animal.Animal;
import personaje.Personaje;

public class SuperPocima extends Pocima {

	@Override
	public void aplicarPocimaConSusEfectos(Animal animal, Personaje personaje) {
		animal.incrementoEnergiaSuperPocima(8);

	}

	@Override
	public TipoDePocima getTipoPocima() {
		return TipoDePocima.SUPER_POCIMA;
	}

}
