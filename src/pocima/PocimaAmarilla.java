package pocima;

import animal.Animal;
import personaje.Personaje;

public class PocimaAmarilla extends Pocima {

	@Override
	public void aplicarPocimaConSusEfectos(Animal animal, Personaje personaje) {
		animal.disminuirEnergia(10);
		animal.disminuirFuerzaVinculoConDuenio(3);
	}

	@Override
	public TipoDePocima getTipoPocima() {
		return TipoDePocima.AMARILLAS;
	}

}
