package pocima;

import animal.Animal;
import personaje.Personaje;

public class PocimaRoja extends Pocima {

	@Override
	public void aplicarPocimaConSusEfectos(Animal animal, Personaje personaje) {
		if (personaje.esElDuenio(animal)) {
			animal.aumentarEnergia(4);
		} else {
			if (!personaje.esElDuenio(animal) || animal.tieneComoDuenioA(null)) {
				animal.disminuirResistencia(3);
			}
		}
	}

	@Override
	public TipoDePocima getTipoPocima() {
		return TipoDePocima.ROJAS;
	}

}
