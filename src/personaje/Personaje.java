package personaje;

import java.util.ArrayList;
import java.util.List;

import animal.Animal;
import animal.TipoAnimal;
import criterio.Criterio;
import pocima.Pocima;
import pocima.TipoDePocima;
import varita.Varita;

public class Personaje {

	private static int limiteSabiduria = 30;
	private int sabiduria;
	public Criterio criterio1;
	public double superEnergia;

	List<Pocima> pocimas = new ArrayList<>();
	List<Animal> losAnimalesDelPersonaje = new ArrayList<>();
	Varita varita;

	public void agregarPocima(Pocima pocima) {
		pocimas.add(pocima);

	}

	public List<Pocima> getPocimas() {
		return pocimas;
	}

	public int getCantidadDePocimasDelPersonaje() {
		return pocimas.size();
	}

	public void setLimiteSabiduria(int limiteSabiduria) {
		Personaje.limiteSabiduria = limiteSabiduria;
	}

	public int getSabiduria() {
		return sabiduria;
	}

	public void setSabiduria(int sabiduria) {
		this.sabiduria = sabiduria;
	}

	public int getLimiteSabiduria() {
		return limiteSabiduria;

	}

	public int incrementarSabiduria(int sabiduriaPersonaje) {
		int sabi = sabiduria + sabiduriaPersonaje;
		sabiduria = Math.min(limiteSabiduria, sabi);
		return sabiduria;
	}

	public void disminuirSabiduria(int sabiduriaPersonaje) {
		int sabi = this.getSabiduria() - sabiduriaPersonaje;
		this.sabiduria = Math.max(sabi, 0);
		this.setSabiduria(sabiduria);
	}

	public void aplicarPocima(TipoDePocima tipo, Animal animal) {
		Pocima pocima = this.buscarPocima(tipo);
		pocima.aplicarPocimaConSusEfectos(animal, this);
		pocimas.remove(pocima);
	}

	public Pocima buscarPocima(TipoDePocima tipo) {
		return pocimas.stream().filter(p -> p.getTipoPocima().equals(tipo)).findAny().get();
	}

	public boolean tienePocimaDelTipo(TipoDePocima tipo) {
		return pocimas.stream().anyMatch(p -> p.getTipoPocima().equals(tipo));
	}

	public double incrementoFuerzaVinculoPorAplicarVarita(Animal animal) {
		return animal.incrementarLaFuerzaDelVinculoConElDuenio(this.getSabiduria() / 3);
	}

	public void setVarita(Varita varitas) {
		varita = varitas;
	}

	public Varita getVarita() {
		return varita;
	}

	public void aplicarLaVarita(Animal animal) {
		varita.aplicarVarita(animal, this);
	}

	public void setCriterio(Criterio criterio) {
		this.criterio1 = criterio;

	}

	public Criterio getCriterio() {
		return criterio1;
	}

	public void agregarLosAnimalesDelPersonaje(Animal losAnimalesDelPersonaje) {
		this.losAnimalesDelPersonaje.add(losAnimalesDelPersonaje);
	}

	public List<Animal> getLosAnimalesDelPersonaje() {
		return losAnimalesDelPersonaje;
	}

	public boolean esElDuenio(Animal animal) {
		return losAnimalesDelPersonaje.stream().anyMatch(a -> a.getAnimal().equals(animal));
	}

	public long getFortalezaPersonaje() {
		long totalFortalesza = (long) (this.getSabiduria() + this.getEnergiaAnimalesDelPersonajes());
		return totalFortalesza;
	}

	public double getEnergiaAnimalesDelPersonajes() {
		return losAnimalesDelPersonaje.stream().mapToDouble(a -> a.getEnergia()).sum();
	}

	public void encuentroConAnimal(Animal animal) {
		this.criterio1.aplicarCriterio(animal, this);
		animal.encuentroAnimalPersonaje(this);
	}

}
