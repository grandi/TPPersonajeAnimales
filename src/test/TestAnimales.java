package test;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import animal.Buey;
import animal.Hiena;
import animal.Perro;
import castillo.CastilloAgricola;
import castillo.CastilloMaligno;
import castillo.CastilloResistente;
import criterio.Criterio;
import criterio.CriterioA;
import criterio.CriterioB;
import criterio.CriterioC;
import personaje.Personaje;
import varita.VaritaConEfecto;
import varita.VaritaSinEfecto;

public class TestAnimales {

	private Personaje pedro;
	private Personaje jose;
	private Personaje lara;
	private Personaje juan;
	private Personaje vale;

	private Buey buey;
	private Hiena hiena;
	private Perro perro;

	private CastilloAgricola elAgricultor;
	private CastilloMaligno laMaldad;
	private CastilloResistente laResistencia;

	private Criterio criterioA;
	private CriterioB criterioB;
	private CriterioC criterioC;
	private VaritaConEfecto varitaConEfecto;

	@Before
	public void setUp() {
		jose = new Personaje();
		pedro = new Personaje();
		juan = new Personaje();
		vale = new Personaje();
		lara = new Personaje();
		buey = new Buey(5, 5);
		hiena = new Hiena(6, 2);
		perro = new Perro(5, 12);

		elAgricultor = new CastilloAgricola(6, 6);
		laMaldad = new CastilloMaligno();
		laResistencia = new CastilloResistente();

		criterioA = new CriterioA();
		criterioB = new CriterioB();
		criterioC = new CriterioC();
		varitaConEfecto= new VaritaConEfecto();
	}

	//En la parte de Animales en el buey, donde dice que acepta como duenio al personaje que le haga subir
	//la sabiduria, el criterio que le hace subir la sabiduria esta el B  el la parte de  aplicar varita,
	//en el cual al aplicar la varita con efecto la
	//sabiduria se veria incrementada, pero la varaita se aplica solo si el animal es propio; por lo tanto el animal no deberia 
	// cambiar de duenio sino q seguiria con el mismo duenio;
	
	

	@Test
	public void elBueySeQuedaSinDuenio() {
		laMaldad.setCapacidadParaCrearPocima(5);
		laMaldad.registroDelPasoDelPersonajePorElCastillo(pedro);
		buey.setEnergia(15);
		buey.setFuerzaDeVinculo(6);
		buey.setDuenio(juan);
		juan.agregarLosAnimalesDelPersonaje(buey);
		assertTrue(juan.esElDuenio(buey));
		assertEquals(1, juan.getLosAnimalesDelPersonaje().size());
		pedro.setCriterio(criterioB);
		pedro.encuentroConAnimal(buey);
		assertEquals(3, buey.getFuerzaDeVinculo(), 0.0001);
		assertEquals(0, juan.getLosAnimalesDelPersonaje().size());
		assertFalse(juan.esElDuenio(buey));
	}

	@Test
	public void laHienaDejaASuDuenioPorqueSuEnergiaPasoASerMultiploDeTres() {
		hiena.setEnergia(19);
		hiena.setDuenio(pedro);
		pedro.agregarLosAnimalesDelPersonaje(hiena);
		assertTrue(pedro.esElDuenio(hiena));

		laMaldad.setCapacidadParaCrearPocima(5);
		laMaldad.registroDelPasoDelPersonajePorElCastillo(juan);
		juan.setCriterio(criterioB);
		juan.encuentroConAnimal(hiena);

		assertEquals(9, hiena.getEnergia(), 0.001);
		assertFalse(pedro.esElDuenio(hiena));
	}

	@Test
	public void laHienaAceptaComoDuenioAJoseQueLeDa8PuntosDeEnergiaPorqueTieneSuperPocima() {
		laResistencia.setCapacidadParaCrearPocima(25);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(pedro);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(juan);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(vale);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(jose);
		assertEquals(6, hiena.getEnergia(), 0.001);
		hiena.setDuenio(pedro);
		pedro.agregarLosAnimalesDelPersonaje(hiena);
		jose.setCriterio(criterioC);
		jose.encuentroConAnimal(hiena);
		assertEquals(14, hiena.getEnergia(), 0.001);
		assertTrue(jose.esElDuenio(hiena));
	}
	@Test
	public void elPerroAbandonaASuDuenioPorqueLaFuerzaDElVinculoBajaA0(){
		laMaldad.setCapacidadParaCrearPocima(5);
		laMaldad.registroDelPasoDelPersonajePorElCastillo(jose);
		perro.setDuenio(jose);
		jose.agregarLosAnimalesDelPersonaje(perro);
		assertTrue(jose.esElDuenio(perro));
		juan.setCriterio(criterioB);
		juan.encuentroConAnimal(perro);
		assertFalse(jose.esElDuenio(perro));
		
	}
	
	//Para el caso del perro que acepta como duenio al personaje que le lleve la fuerza del vinculo a 5 o mas
	//lo unico que hace incremetar la fuerza del vinculo es la aplicacion de la varita con efecto en el encuentro con
	//el animal, pero solo se aplica a los animales que tienen duenio, por lo tanto el duenio va a seguir siendo el mismo.
	@Test 
	public void elPerroAceptaComoDuenioAQuienLeSubaLaFuerzaDelVinculoA5oMas(){
		perro.setDuenio(pedro);
		pedro.agregarLosAnimalesDelPersonaje(perro);
		pedro.setSabiduria(15);
		assertEquals(0,perro.getFuerzaDeVinculo(),0.001);
		pedro.setVarita(varitaConEfecto);
		pedro.setCriterio(criterioB);
		pedro.encuentroConAnimal(perro);
		assertEquals(5, perro.getFuerzaDeVinculo(),0.001);
		assertTrue(perro.tieneComoDuenioA(pedro));
	}
	
}