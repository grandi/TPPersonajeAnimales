package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import animal.Buey;
import animal.Hiena;
import animal.Perro;
import castillo.CastilloAgricola;
import castillo.CastilloMaligno;
import castillo.CastilloResistente;
import criterio.CriterioA;
import criterio.CriterioB;
import criterio.CriterioC;
import juego.Juego;
import personaje.Personaje;
import varita.VaritaConEfecto;
import varita.VaritaSinEfecto;

public class TestRequerimientos {
	private Personaje juan;
	private Personaje pedro;
	private Personaje jose;
	private Buey buey;
	private Hiena hiena;
	private Perro perro;
	private CastilloAgricola elAgricultor;
	private CastilloAgricola elAgricultor2;
	private CastilloMaligno laMaldad;
	private CastilloResistente laResistencia;
	private CriterioA criterioA;
	private CriterioB criterioB;
	private Juego elJuego;
	private VaritaConEfecto varitaConEfecto;
	private VaritaSinEfecto varitaSinEfecto;
	private Personaje vale;

	@Before
	public void setUp() {
		juan = new Personaje();
		jose = new Personaje();
		pedro = new Personaje();
		buey = new Buey(5, 5);
		hiena = new Hiena(6, 2);
		perro = new Perro(5, 12);

		elAgricultor = new CastilloAgricola(4, 7);
		elAgricultor2 = new CastilloAgricola(4, 3);
		laMaldad = new CastilloMaligno();
		laResistencia = new CastilloResistente();

		criterioB = new CriterioB();
		criterioA = new CriterioA();
		varitaConEfecto = new VaritaConEfecto();
		varitaSinEfecto = new VaritaSinEfecto();
		elJuego = new Juego();

		vale = new Personaje();
		buey.setDuenio(vale);
		hiena.setDuenio(jose);
	}

	/////////////// PUNTO 1 PASO DEL PERSONAJE POR EL
	/////////////// CASTILLO//////////////////////////////////////

	@Test
	public void elPersonajePedroPasoPorElCastillo() {
		elAgricultor.personajeQuePasaPorCastillo(pedro);
		assertTrue(elAgricultor.elPersonajePasoPorElCastillo(pedro));
	}

	@Test
	public void elPersonajeNoPasoPorElCastillo() {
		elAgricultor.personajeQuePasaPorCastillo(juan);
		assertFalse(elAgricultor.elPersonajePasoPorElCastillo(pedro));
	}

	@Test
	public void capacidadProduccionPocimas2() {
		elAgricultor.setCapacidadParaCrearPocima(2);
		assertEquals(2, elAgricultor.getCapacidadParaCrearPocima());
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(juan);
		assertEquals(1, elAgricultor.getCapacidadParaCrearPocima());
		assertTrue(elAgricultor.puedeCrearPocima());

	}

	@Test
	public void elCastilloNoTieneCapacidadDeProduccion() {
		elAgricultor.setCapacidadParaCrearPocima(1);
		assertEquals(1, elAgricultor.getCapacidadParaCrearPocima());
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(juan);
		assertEquals(0, elAgricultor.getCapacidadParaCrearPocima());
		assertFalse(elAgricultor.puedeCrearPocima());

	}

	@Test
	public void registroDelPasoDeJuanPorElCastilloAgricolaPocimaAzulCantidadDePocimas() {
		elAgricultor.setCapacidadParaCrearPocima(2);
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(juan);
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(juan);
		assertEquals(2, juan.getCantidadDePocimasDelPersonaje());
	}

	@Test
	public void registroDelPasoDePedroPorElCastilloMalignoPocimaAmarillaCantidadDePocimas() {
		laMaldad.setCapacidadParaCrearPocima(2);
		assertEquals(2, laMaldad.getCapacidadParaCrearPocima());
		laMaldad.registroDelPasoDelPersonajePorElCastillo(pedro);
		assertEquals(1, pedro.getCantidadDePocimasDelPersonaje());
	}

	@Test
	public void registroDelPasoDePedroPorElCastilloResistentePocimaRoja() {
		laResistencia.setCapacidadParaCrearPocima(2);
		assertEquals(2, laResistencia.getCapacidadParaCrearPocima());
		laResistencia.registroDelPasoDelPersonajePorElCastillo(jose);
		assertEquals(1, jose.getCantidadDePocimasDelPersonaje());
	}

	@Test
	public void registroDelPasoDePedroPorElCastilloResistentePocimaRojaYCastilloMalignoPocimaAmarilla() {
		laResistencia.setCapacidadParaCrearPocima(2);
		laMaldad.setCapacidadParaCrearPocima(3);
		elAgricultor.setCapacidadParaCrearPocima(4);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(pedro);
		laMaldad.registroDelPasoDelPersonajePorElCastillo(pedro);
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(pedro);
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(juan);
		assertEquals(3, pedro.getCantidadDePocimasDelPersonaje());
	}

	@Test
	public void verificacionSabiduriaDespuesDePasarPorElCastilloResistente() {
		laResistencia.setCapacidadParaCrearPocima(2);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(jose);
		assertEquals(2, jose.getSabiduria());
	}

	@Test
	public void verificacionSabiduriaTotalDespuesDePasarPorElCastilloAgricultor3VecesConLimiteSabiruria7() {
		jose.setLimiteSabiduria(7);
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(jose);
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(jose);
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(jose);
		assertEquals(6, jose.getSabiduria());

	}

	@Test
	public void testeandoLaSabiduriaDelPersonajeJuanConLimiteDe5() {
		assertEquals(0, juan.getSabiduria());
		juan.setLimiteSabiduria(5);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(juan);
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(juan);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(juan);
		assertEquals(5, juan.getSabiduria());
	}

	/////////////// PUNTO 2 ENCUENTRO DE UN PERSONAJE CON UN
	/////////////// ANIMAL/////////////////

	@Test
	public void encuentroPersonajeConPerroDeOtroDuenioAplicaPocimaAzul() {
		elAgricultor.setCapacidadParaCrearPocima(5);
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(jose);
		assertEquals(12, perro.getResistencia(), 0.0001);
		assertEquals(5, perro.getEnergia(), 0.0001);
		perro.setDuenio(pedro);
		jose.setCriterio(criterioA);
		jose.encuentroConAnimal(perro);
		assertEquals(5, perro.getResistencia(), 0.0001);

	}

	@Test
	public void encuentroPersonajeAnimalNoAplicaPocima() {
		laMaldad.registroDelPasoDelPersonajePorElCastillo(pedro);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(pedro);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(pedro);
		assertEquals(0, pedro.getCantidadDePocimasDelPersonaje());
		assertEquals(12, perro.getResistencia(), 0.0001);
		pedro.setCriterio(criterioA);
		perro.setDuenio(jose);
		pedro.encuentroConAnimal(perro);
		assertEquals(12, perro.getResistencia(), 0.0001);
		assertEquals(5, perro.getEnergia(), 0.0001);
	}

	@Test
	public void encuentroPersonajeConPerroDeOtroDuenioResistencia7AplicaPocimaAzulDisminucionResitencia3() {
		elAgricultor2.setCapacidadParaCrearPocima(5);
		elAgricultor2.registroDelPasoDelPersonajePorElCastillo(pedro);
		Perro perro1 = new Perro(8, 7);
		assertEquals(7, perro1.getResistencia(), 0.001);
		perro1.setDuenio(vale);
		pedro.setCriterio(criterioA);
		pedro.encuentroConAnimal(perro1);
		assertEquals(4, perro1.getResistencia(), 0.001);
	}

	@Test
	public void aplicarCriterioBVaritaConEfectoAnimalesPropio() {

		Hiena hiena1 = new Hiena(7, 5);
		vale.agregarLosAnimalesDelPersonaje(hiena1);
		vale.setSabiduria(9);
		hiena1.setFuerzaDeVinculo(6);
		vale.setVarita(varitaConEfecto);
		vale.setCriterio(criterioB);
		vale.encuentroConAnimal(hiena1);
		assertEquals(9, hiena1.getFuerzaDeVinculo(), 0.001);

	}

	@Test
	public void aplicarCriterioBAnimalesAjeno() {
		laMaldad.setCapacidadParaCrearPocima(5);
		laMaldad.registroDelPasoDelPersonajePorElCastillo(pedro);
		buey.setEnergia(15);
		buey.setFuerzaDeVinculo(8);

		pedro.setVarita(varitaConEfecto);
		pedro.setCriterio(criterioB);
		pedro.encuentroConAnimal(buey);
		assertEquals(5, buey.getEnergia(), 0.0001);
		assertEquals(5, buey.getFuerzaDeVinculo(), 0.001);
	}

	@Test
	public void aplicarCriterioBAnimalesPropioFuerzaVinculo8Sabiduria9() {
		buey.setEnergia(15);
		buey.setFuerzaDeVinculo(8);
		pedro.setSabiduria(9);
		buey.setDuenio(pedro);
		pedro.agregarLosAnimalesDelPersonaje(buey);
		pedro.setVarita(varitaConEfecto);
		pedro.setCriterio(criterioB);
		pedro.encuentroConAnimal(buey);
		assertEquals(11, buey.getFuerzaDeVinculo(), 0.001);
	}

	@Test
	public void aplicarVaritaSinEfectoConPocimaAmarillaAnimalAjeno() {
		laMaldad.setCapacidadParaCrearPocima(5);
		laMaldad.registroDelPasoDelPersonajePorElCastillo(pedro);
		assertEquals(5, buey.getEnergia(), 0.001);
		pedro.setVarita(varitaSinEfecto);
		pedro.setCriterio(criterioB);
		pedro.encuentroConAnimal(buey);
		assertEquals(0, buey.getEnergia(), 0.0001);
	}

	@Test
	public void aplicarVaritaSinEfectoAjeno() {
		assertEquals(5, buey.getEnergia(), 0.001);
		pedro.setVarita(varitaSinEfecto);
		pedro.setCriterio(criterioB);
		pedro.encuentroConAnimal(buey);
		assertEquals(5, buey.getEnergia(), 0.0001);
	}

	///////// Punto 3 recargar un castillo///////////////////
	@Test
	public void recargaDeCastillo() {
		elAgricultor.setCapacidadParaCrearPocima(2);
		elJuego.recargarCastillo(elAgricultor);
		assertEquals(8, elAgricultor.getCapacidadParaCrearPocima());

	}

	////////// PUNTO 4 QUE EL JUEGO LE PIDA UN ANIMAL A CADA
	////////// CASTILLO////////////////
	@Test
	public void cadaCastilloCreaUnAnimal() {
		elJuego.setLosCastillosDelJuego(elAgricultor);
		elJuego.setLosCastillosDelJuego(laMaldad);
		elJuego.setLosCastillosDelJuego(laResistencia);
		elJuego.crearAnimal();
		assertEquals(3, elJuego.crearAnimal().size());

	}

	////////////////// PUNTO 5 FORTALEZA DEL PERSONAJE///////////////////
	@Test
	public void fortalezaDelPersonajeCon3Animales() {
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(juan);
		juan.agregarLosAnimalesDelPersonaje(buey);
		juan.agregarLosAnimalesDelPersonaje(hiena);
		assertEquals(13, juan.getFortalezaPersonaje());
	}

	//////////// PUNTO 6 ANTIGUEDAD DEL JUEGO///////////////////
	@Test
	public void antiguedadDelJuego() {
		elJuego.setLosCastillosDelJuego(elAgricultor);
		elJuego.setLosCastillosDelJuego(laMaldad);
		elJuego.setLosCastillosDelJuego(laResistencia);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(juan);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(jose);
		laResistencia.registroDelPasoDelPersonajePorElCastillo(pedro);
		laMaldad.registroDelPasoDelPersonajePorElCastillo(juan);
		laMaldad.registroDelPasoDelPersonajePorElCastillo(jose);
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(juan);
		elAgricultor.registroDelPasoDelPersonajePorElCastillo(pedro);
		assertEquals(7, elJuego.getAntiguedadDelJuego());
	}

}