package varita;

import animal.Animal;
import personaje.Personaje;

public class VaritaConEfecto extends Varita {

	@Override
	public void aplicarVarita(Animal animal, Personaje personaje) {
			personaje.incrementoFuerzaVinculoPorAplicarVarita(animal);
		
	}


	
}
