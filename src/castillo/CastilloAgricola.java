package castillo;

import animal.Animal;
import animal.Buey;
import personaje.Personaje;
import pocima.Pocima;
import pocima.PocimaAzul;
import pocima.TipoDePocima;

public class CastilloAgricola extends Castillo {
	public double incrementoResistenciaAnimalPropio;
	public double disminucionResistenciaAnimalAjeno;
	private double incremento;

	public CastilloAgricola(double incrementoResistnciaAnimalPropio, double disminucionResistenciaAnimalAjeno) {
		this.incrementoResistenciaAnimalPropio = incrementoResistnciaAnimalPropio;
		this.disminucionResistenciaAnimalAjeno = disminucionResistenciaAnimalAjeno;
	}

	public double configuracionResistenciaAnimalPropio() {
		return this.incrementoResistenciaAnimalPropio;
	}

	public double configuracionResistenciaAnimalAjeno() {
		return this.disminucionResistenciaAnimalAjeno;
	}

	@Override
	public Pocima crearLaPocima(Personaje personaje) {
		Pocima laPocima = new PocimaAzul(incrementoResistenciaAnimalPropio, disminucionResistenciaAnimalAjeno);
		lasPocimas.add(laPocima);
		return laPocima;

	}

	public double incrementoPorSabiduriaAlta(Personaje personaje) {
		incremento = personaje.getSabiduria() > 20 ? 3 : 0;
		return incremento;
	}

	public double getIncrementoResistenciaAnimalPropio() {
		return incrementoResistenciaAnimalPropio;
	}

	public double getDisminucionResistenciaAnimalAjeno() {
		return disminucionResistenciaAnimalAjeno;
	}

	@Override
	public TipoCastillo getTipoCastillo() {
		return TipoCastillo.CASTILLO_AGRICOLA;
	}

	@Override
	public void modificarSabiduria(Personaje personaje) {
		personaje.incrementarSabiduria(2);
	}

	@Override
	public Animal cadaCastilloCreaSuAnimal() {
		return new Buey(5, 5);
	}

}