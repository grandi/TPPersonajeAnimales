package castillo;

import animal.Animal;
import animal.Hiena;
import personaje.Personaje;
import pocima.Pocima;
import pocima.PocimaAmarilla;

public class CastilloMaligno extends Castillo {


	@Override
	public Pocima crearLaPocima(Personaje personaje) {
		Pocima laPocima = new PocimaAmarilla();
		lasPocimas.add(laPocima);
		return laPocima;
	}

	@Override
	public TipoCastillo getTipoCastillo() {
		return TipoCastillo.CASTILLO_MALIGNO;
	}

	@Override
	public void modificarSabiduria(Personaje personaje) {
		personaje.disminuirSabiduria(5);
	}

	@Override
	public Animal cadaCastilloCreaSuAnimal() {
		return new Hiena(6, 5);
	}

}
