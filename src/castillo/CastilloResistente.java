package castillo;

import animal.Animal;
import animal.Buey;
import animal.Perro;
import personaje.Personaje;
import pocima.Pocima;
import pocima.PocimaRoja;
import pocima.SuperPocima;
import pocima.TipoDePocima;

public class CastilloResistente extends Castillo {

	public Pocima laPocima;
	public TipoDePocima pocima;


	@Override
	public Pocima crearLaPocima(Personaje personaje) {
		if (this.getCantidadPersonasQuePasaronPorElCastillo() % 2 == 0) {
			laPocima = new SuperPocima();

		} else {
			laPocima = new PocimaRoja();
			lasPocimas.add(laPocima);
		}

		return laPocima;
	}

	@Override
	public TipoCastillo getTipoCastillo() {
		return TipoCastillo.CASTILLO_RESISTENTE;
	}

	@Override
	public void modificarSabiduria(Personaje personaje) {
		personaje.incrementarSabiduria(2);
	}

	@Override
	public Animal cadaCastilloCreaSuAnimal() {
		Animal animal = (getCantidadPersonasQuePasaronPorElCastillo() % 2 == 0) ? new Buey(5, 12)
				: new Perro(getCantidadPersonasQuePasaronPorElCastillo(), getCantidadPersonasQuePasaronPorElCastillo());
		return animal;
	}

}
