package castillo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import animal.Animal;
import juego.Juego;
import personaje.Personaje;
import pocima.Pocima;
import pocima.TipoDePocima;

public abstract class Castillo extends Juego {

	public int capacidadParaCrearPocima;
	Set<Personaje> losPersonajesQuePasaron = new HashSet<>();
	List<Pocima> lasPocimas = new ArrayList<>();

	public int cantidadPersonasDelCastillo;

	public boolean puedeCrearPocima() {
		return this.capacidadParaCrearPocima >= 1;
	}

	public int getCapacidadParaCrearPocima() {
		return this.capacidadParaCrearPocima;
	}

	public void setCapacidadParaCrearPocima(int capacidadParaProducirPocimas) {
		this.capacidadParaCrearPocima = capacidadParaProducirPocimas;
	}

	public List<Pocima> getLasPocimasDelCastillo() {
		return lasPocimas;
	}

	public void registroDelPasoDelPersonajePorElCastillo(Personaje personaje) {
		this.modificarSabiduria(personaje);
		this.personajeQuePasaPorCastillo(personaje);
		if (puedeCrearPocima()) {
			this.capacidadParaCrearPocima--;
			personaje.agregarPocima(crearLaPocima(personaje));
		}
	}

	public abstract Pocima crearLaPocima(Personaje personaje);

	public abstract void modificarSabiduria(Personaje personaje);

	public Set<Personaje> getLosPersonajesQuePasaron() {
		return losPersonajesQuePasaron;
	}

	public void personajeQuePasaPorCastillo(Personaje personaje) {
		this.losPersonajesQuePasaron.add(personaje);
	}

	public boolean elPersonajePasoPorElCastillo(Personaje personaje) {
		return this.losPersonajesQuePasaron.contains(personaje);
	}

	public abstract TipoCastillo getTipoCastillo();

	public abstract Animal cadaCastilloCreaSuAnimal();

	public int getCantidadPersonasQuePasaronPorElCastillo() {
		return losPersonajesQuePasaron.size();
	}

}