package juego;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import animal.Animal;
import castillo.Castillo;

public class Juego {
	public List<Castillo> losCastillos = new ArrayList<>();
	public Animal Animal;
	
	List<Animal> losAnimalesDelJuego = new ArrayList<>();

	public void setLosCastillosDelJuego(Castillo castillo) {
		losCastillos.add(castillo);
	}

	public List<Castillo> getLosCastillos() {
		return losCastillos;
	}

	public void setLosAnimalesDelCastillo(List<Animal> losAnimalesDelCastillo) {
		this.losAnimalesDelJuego = losAnimalesDelCastillo;
	}

	public int getAntiguedadDelJuego() {
		return losCastillos.stream().mapToInt(c -> c.getCantidadPersonasQuePasaronPorElCastillo()).sum();
	}

	public void recargarCastillo(Castillo casti) {
		casti.capacidadParaCrearPocima += 6;
	}

	public List<Animal> getLosAnimalesDelCastillo() {
		return losAnimalesDelJuego;
	}

	public List<Animal> crearAnimal() {
		return losCastillos.stream().map(c -> c.cadaCastilloCreaSuAnimal()).collect(Collectors.toList());
	}

}