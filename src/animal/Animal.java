package animal;

import personaje.Personaje;

public abstract class Animal {

	public double energia;
	public double resistencia;
//	public double resistenciaModificada;
//	public double energiaModificada;
	public double fuerzaDeVinculo;
	public double incremento;
	public Personaje personaje;
	public double fuerza;

	public Animal(double energia, double resistencia) {
		this.energia = energia;
		this.resistencia = resistencia;
	}

	public Personaje getDuenio() {
		return this.personaje;
	}

	public void setDuenio(Personaje personaje) {
		this.personaje = personaje;
	}

	public double getEnergia() {
		return energia;
	}

	public void setEnergia(double energia) {
		this.energia = energia;
	}

	public double getResistencia() {
		return resistencia;
	}

	public void setResistencia(double resistencia) {
		this.resistencia = resistencia;
	}

	public double getFuerzaDeVinculo() {
		return fuerzaDeVinculo;
	}

	public void setFuerzaDeVinculo(double fuerzaDeVinculo) {
		this.fuerzaDeVinculo = fuerzaDeVinculo;
	}

	public double disminuirEnergia(double ener) {
		energia = energia - ener;
		energia = Math.max(energia, 0);
		return energia;
	}

	public void disminuirResistencia(double resist) {
		resistencia = resistencia - resist;
		resistencia = Math.max(resistencia, 0);
	}

	public double aumentarEnergia(double ener) {
		return energia = energia + ener;

	}
	public double incrementoEnergiaSuperPocima(double incremento){
		this.incremento=incremento;
		 return energia = energia + incremento;
	}
	public double incrementoEnergiaSuperPocima(){
		return this.incremento;
	}
	public double aumentarResistencia(double resist) {
		resistencia = resistencia + resist;
		return resistencia;
	}

	public double disminuirFuerzaVinculoConDuenio(double fuerza) {
		this.fuerza=fuerza;
		fuerzaDeVinculo = fuerzaDeVinculo - fuerza;
		fuerzaDeVinculo = Math.max(fuerzaDeVinculo, 0);
		return fuerzaDeVinculo;
	}
	public double disminucionFuerzaVinculo(){
		return fuerza;
	}

	public double incrementarLaFuerzaDelVinculoConElDuenio(double incremento) {
		this.incremento = incremento;
		return this.fuerzaDeVinculo = fuerzaDeVinculo + this.incremento;
	}
	public double incrementoFuerzaVinculo(){
		return incremento;
	}

	public abstract TipoAnimal getTipoAnimal();

	public Animal getAnimal() {
		return this;
	}

	public abstract void abandonarDuenio(Personaje personaje);

	public boolean tieneComoDuenioA(Personaje personaje) {
		return this.getDuenio().equals(personaje);

	}

	public void encuentroAnimalPersonaje(Personaje personaje) {
		this.abandonarDuenio(personaje);

	}

//	public Personaje elDuenioDelAnimal() {
//		return this.getDuenio();
//	}

}
