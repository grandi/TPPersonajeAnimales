package animal;

import personaje.Personaje;

public class Hiena extends Animal {

	public Hiena(double energia, double resistencia) {
		super(energia, resistencia);
	}

	@Override
	public TipoAnimal getTipoAnimal() {
		return TipoAnimal.HIENA;
	}

	@Override
	public void abandonarDuenio(Personaje personaje) {
		if (this.getEnergia() % 3 == 0) {
			this.getDuenio().getLosAnimalesDelPersonaje().remove(this);
			this.setDuenio(null);
		} else {
			if (this.incrementoEnergiaSuperPocima() >= 8) {
				this.getDuenio().getLosAnimalesDelPersonaje().remove(this);
				this.setDuenio(personaje);
				personaje.agregarLosAnimalesDelPersonaje(this);
			}
		}

	}

}
