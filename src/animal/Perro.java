package animal;

import personaje.Personaje;

public class Perro extends Animal {

	public Perro(double energia, double resistencia) {
		super(energia, resistencia);
	}

	@Override
	public TipoAnimal getTipoAnimal() {
		return TipoAnimal.PERRO;
	}

	@Override
	public void abandonarDuenio(Personaje personaje) {
		if (this.getFuerzaDeVinculo() == 0) {
			this.getDuenio().getLosAnimalesDelPersonaje().remove(this);
			this.setDuenio(null);
		} 
			if (this.getFuerzaDeVinculo() >= 5) {
				this.getDuenio().getLosAnimalesDelPersonaje().remove(this);
				this.setDuenio(personaje);
				personaje.agregarLosAnimalesDelPersonaje(this);
				personaje.incrementarSabiduria(1);
			}
		}

}
