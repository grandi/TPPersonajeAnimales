package animal;

import personaje.Personaje;

public class Buey extends Animal {


	public Buey(double energia, double resistencia) {
		super(energia, resistencia);

	}

	@Override
	public TipoAnimal getTipoAnimal() {
		return TipoAnimal.BUEY;
	}

	@Override
	public void abandonarDuenio(Personaje personaje) {

		if (this.disminucionFuerzaVinculo() < 5) {
			this.getDuenio().getLosAnimalesDelPersonaje().remove(this);
			this.setDuenio(null);
		} else {
			if (this.incrementoFuerzaVinculo() > 10) {
				this.getDuenio().getLosAnimalesDelPersonaje().remove(this);
				this.setDuenio(personaje);
				personaje.agregarLosAnimalesDelPersonaje(this);
			}
		}
	}

	@Override
	public void disminuirResistencia(double resist) {
		if (resist >= 4) {
			super.disminuirResistencia(resist);
		}
	}

	public Animal elAnimalDelPersonaje() {
		return personaje.getLosAnimalesDelPersonaje().stream().filter(p -> p.getAnimal().equals(this)).findAny().get();
	}

}