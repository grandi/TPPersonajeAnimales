package criterio;

import animal.Animal;
import personaje.Personaje;
import pocima.TipoDePocima;

public class CriterioC extends Criterio {

	@Override
	public void aplicarCriterio(Animal animal, Personaje personaje) {
		if (personaje.tienePocimaDelTipo(TipoDePocima.SUPER_POCIMA)) {
			personaje.aplicarPocima(TipoDePocima.SUPER_POCIMA, animal);
		}

	}
}