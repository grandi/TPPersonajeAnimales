package criterio;

import animal.Animal;
import personaje.Personaje;

public abstract class Criterio {

	public abstract  void aplicarCriterio(Animal animal, Personaje personaje);
		

}
