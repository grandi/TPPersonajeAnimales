package criterio;

import animal.Animal;
import personaje.Personaje;
import pocima.TipoDePocima;

public class CriterioB extends Criterio {

	@Override
	public void aplicarCriterio(Animal animal, Personaje personaje) {
		if (personaje.esElDuenio(animal)) {
			personaje.aplicarLaVarita(animal);
		} 
		else {
			if (!personaje.esElDuenio(animal) || animal.getDuenio() == null) {
				if (personaje.tienePocimaDelTipo(TipoDePocima.AMARILLAS)) {
					personaje.aplicarPocima(TipoDePocima.AMARILLAS, animal);
				}
			}
		}
	}

}
