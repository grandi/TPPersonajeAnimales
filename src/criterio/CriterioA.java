package criterio;

import animal.Animal;
import personaje.Personaje;
import pocima.TipoDePocima;

public class CriterioA extends Criterio {

	@Override
	public void aplicarCriterio(Animal animal, Personaje personaje) {
		if (personaje.tienePocimaDelTipo(TipoDePocima.AZULES)) {
			personaje.aplicarPocima(TipoDePocima.AZULES, animal);
		}
		if (personaje.tienePocimaDelTipo(TipoDePocima.ROJAS)) {
			personaje.aplicarPocima(TipoDePocima.ROJAS, animal);
		}

	}

}
